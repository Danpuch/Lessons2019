class Coche():

    def __init__(self):

        self.__largoChasis = 250
        self.__anchoChasis = 120
        self.__ruedas = 4
        self.__enmarcha=False

    def arrancar(self, arrancamos):
        self.__enmarcha=arrancamos
        if (self.__enmarcha==True):
            return "El coche esta en marcha"
        else:
            return "El coche esta parado"

    def estado(self):
        print("El coche tiene", self.__ruedas, "ruedas")
        print("Un ancho de", self.__anchoChasis, "y largo de", self.__largoChasis)

miCoche=Coche()
print(miCoche.arrancar(True))
miCoche.estado()

print("**************** A continuación cremanos el segundo objeto ***********************")

mi2Coche =Coche()
print(mi2Coche.arrancar(False))
mi2Coche.estado()




