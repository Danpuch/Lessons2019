class Coche():

    def __init__(self):

        self.largoChasis = 250
        self.anchoChasis = 120
        self.ruedas = 4
        self.enmarcha=False

    def arrancar(self, arrancamos):
        self.enmarcha=arrancamos
        if (self.enmarcha==True):
            return "El coche esta en marcha"
        else:
            return "El coche esta parado"

    def estado(self):
        print("El coche tiene", self.ruedas, "ruedas")
        print("Un ancho de", self.anchoChasis, "y largo de", self.largoChasis)

miCoche=Coche()

print(miCoche.arrancar(True))
miCoche.estado()

print("**************** A continuación cremanos el segundo objeto ***********************")

mi2Coche =Coche()
print(mi2Coche.arrancar(False))
mi2Coche.estado()




