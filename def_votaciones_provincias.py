'''carga votos de 3 candidatos.
en una lista, primer componente el nombre. y en la segunda componente cargar
una lista con componentes de tipo tupla con el nombre de la provincia y la cantidad
de votos obtenidos en la provincia.
Se deben cargar los datos por teclado'''

def carga_datos():
    listaelecciones=[]
    for i in range(3):
        name = input("Ingresa nombre de candidato: ")
        num_provincias = int(input("Cuantas provincias quiere cargar?: "))
        lprovotos=[]
        for x in range(num_provincias):
            provincia = input("Nombre provincia: ")
            votos = int(input("Votos obtenidos: "))
            lprovotos.append((provincia, votos))
        listaelecciones.append((name, lprovotos))
    return listaelecciones

def imprimir_resultados(listafiltro):
    for i in range(len(listafiltro)):
        sumatorio=0
        for x in range(len(listafiltro[i][1])):
            sumatorio+=listafiltro[i][1][x][1]
        print(listafiltro[i][0],sumatorio)

lista1=carga_datos()
imprimir_resultados(lista1)